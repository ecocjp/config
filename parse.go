/*
	config provide a convenient interface for reading configuration files
 like ACE ones.

	An ACE configuration file is an ini-like file with a syntax that obey to
 following rules.
 	- One or many section [name]. Section groups configuration options together.
	  Many sections of same name are allowed. Sections with same name are numbered
	  from 0 to n.
	- Each section must have one or more configuration variables. A configuration
	  variable follows syntax: name = value.
	- Value begins on the first non space character following '=' and extends to
	  the next space. A space is a blank, a tab or a carriage return. Values with
	  blanks or tab characters must be enclosed between double quotes.

Exemple:
	[section]
		server=mach1.foo.bar:4321
		user = toto
	[section]
		server=mach2.foo.bar:4321
		user = tata
*/
package config

import (
	"bufio"
	"os"
	"regexp"
	"errors"
	"fmt"
)
//
//	Variable stores configuration variable name = value
//
type Variable struct {
	Name	string
	Value	string
}
//
//	Section keeps tracks of configuration's file section.
//
type Section struct {
	Name		string
	Variables	[]Variable
}
//
//	All sections and variables from configuration file.
//
type Conf struct {
	Sections	[]Section
}
var variable = regexp.MustCompile( "([[:alpha:]]+[[:alnum:]]*)" )
//
// newConf creates a new conf structure.
//
func newConf() *Conf {
	return &Conf{}
}
//
//	addVariable adds a new Variable at end of the last Section in structure
// pointed to by Conf.
//
func (conf *Conf)addVariable( nom, valeur string ) {
	vari := Variable{ Name: nom, Value: valeur }
	section_courante := conf.Sections[len(conf.Sections)-1]
	section_courante.Variables = append( section_courante.Variables, vari )
	conf.Sections[len(conf.Sections)-1] = section_courante
}

// Parse reads configuration file,  parsing every sections in a Conf structures.
//
func Parse( conffile string ) (*Conf, error) {
	file,  err := os.Open(conffile)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	reader := bufio.NewReader( file )
	scanner := bufio.NewScanner( reader )
	scanner.Split( bufio.ScanLines )

	var lineno int = 0

	conf := newConf()

NewLine:
	for scanner.Scan() {
		line := scanner.Text()
		lineno++
		if len(line) == 0 {
			continue
		}
		// Tokenize
		var i int
		for i=0; i < len(line) && (line[i] == ' ' || line[i] ==  '\t' ); i++ {
			continue
		}
		if i == len(line) {		// Only blank on line
			continue NewLine
		}
		if line[i] == '#' {
			continue
		}
		if line[i] == '[' {
			var j int = i
			for line[j] != ']' {
				if j == len(line) {
					return nil, errors.New( fmt.Sprintf( "%d: No ']' at end of section name", lineno ) )
				}
				j++
			}
			section := line[i+1:j]

			sect := Section{ Name: section,  }
			conf.Sections = append( conf.Sections,  sect )

			continue
		}
		var	name string
		var	valeur string
		if variable.MatchString( line ) {
			matched := variable.FindStringSubmatch( line )
			name = matched[0]
		}
		i += len(name)
		for line[i] != '=' {
			if i == len(line) {
				return nil, errors.New( fmt.Sprintf( "%d: Expect '=' insted of EOL after %s", lineno,  name ) )
			}
			i++
		}
		i++
		for ; i < len(line) && (line[i] == ' ' || line[i] ==  '\t' ); i++ {
			if i == len(line) {
				return nil, errors.New( fmt.Sprintf( "%d: Value expected after '='",  lineno ) )
			}
			continue
		}
		var deb int = i
		if line[i] == '"' {
			i++
			for ; i < len(line) && line[i] != '"'; i++ {
				if i == len(line) {
					return nil, errors.New( fmt.Sprintf( "%d: '\"' expected before EOL",  lineno ) )
				}
				if line[i] == '\\' {
					i++
				}
			}
			valeur = line[deb+1:i]
			conf.addVariable( name, valeur )
			continue
		} else {
			for ; i < len(line) && line[i] != ' ' && line[i] != '\t'; i++ {
				if i == len(line) {
					return nil, errors.New( fmt.Sprintf( "%d: value expected before EOL",  lineno ) )
				}
			}
			valeur = line[deb:i]
			conf.addVariable( name, valeur )
			continue
		}

		return nil, errors.New( fmt.Sprintf( "%d: unknown structure: %s", lineno, line ) )
	}

	return conf, nil
}
//
// NbSections returns number of section reads from configuration file.
// For each name given as parameter, the number of corresponding section is
// returned.
//
func (c *Conf)NbSections( ss ...string) []int {
	if len(ss) != 0 {
		nb := make([]int, len(ss))
		for i, s := range ss {
			for _, sect := range c.Sections {
				if sect.Name == s {
					nb[i]++
				}
			}
		}
		return nb
	}
	nb := make([]int, 1)
	nb[0] = len(c.Sections)
	return nb
}
//
// NbVariables returns number of variable for given section. Choice between many
// sections of same name is made by 'instance' number of a particuliar section.
// Section are numbered from 0.
//
func (c *Conf)NbVariables( s string, instance int ) (int, error) {
	i := 0
	if instance < 0 {
		return 0, errors.New( fmt.Sprintf( "Instance number must be > 0" ) )
	}
	for _, sect := range c.Sections {
		if sect.Name == s {
			if i == instance {
				return len( sect.Variables ),  nil
			}
			i++
		}
	}
	return 0, errors.New( fmt.Sprintf( "Section \"%s\" or instance number not found", s ) )
}
//
//	GetVariables returns a slice of all variables corresponding to a section of
// the given instance starting from '0'.
//
func (c *Conf)GetVariables( s string, instance int ) (*[]Variable, error) {
	i:= 0
	if instance < 0 {
		return nil, errors.New( fmt.Sprintf( "Instance number must be > 0" ) )
	}
	for _, sect := range c.Sections {
		if sect.Name == s {
			if i == instance {
				return &sect.Variables, nil
			}
			i++
		}
	}
	return nil, errors.New( fmt.Sprintf( "Section \"%s\" or instance number not found", s ) )
}
//
//	GetVariable return the value of a variable 'v' number 'num' from section 's' of
// a given 'instance'.
//
func (c *Conf)GetVariable( v string, num int, s string, instance int ) (string, error) {
	if( num < 0 ) {
		return "", errors.New( fmt.Sprintf( "Variable instance number must be > 0" ) )
	}
	if( instance < 0 ) {
		return "", errors.New( fmt.Sprintf( "Section instance number must be > 0" ) )
	}
	i := 0
	for _, sect := range c.Sections {
		if sect.Name == s {
			if i == instance {
				j := 0
				for _, vars := range sect.Variables {
					if vars.Name == v {
						if j == num {
							return vars.Value, nil
						}
						j++
					}
				}
			}
			i++
		}
	}

	return "", errors.New( fmt.Sprintf( "Variable %s not found", v ) )
}
/*
func main () {
	conf, err := Parse( "../ldomls/ldomls.conf" )
	if err != nil {
		fmt.Fprintf( os.Stderr, "Parse(): %s.\n",  err )
		os.Exit(1)
	}
	fmt.Fprintf( os.Stderr, "Nb de sections: %d\n", conf.NbSections()[0] )
	fmt.Fprintf( os.Stderr, "Nb de sections: %v\n", conf.NbSections("serveur", "test") )
	nb, err := conf.NbVariables( "test", 0 )
	fmt.Fprintf( os.Stderr, "Nb de variables dans la section \"test\": %d\n", nb )
	nb, err = conf.NbVariables( "serveur", 2 )
	fmt.Fprintf( os.Stderr, "Nb de variables dans la section \"serveur\" 3: %d\n", nb )
	nb, err = conf.NbVariables( "serveur", 7 )
	if err != nil {
		fmt.Fprintf( os.Stderr, "%s\n", err )
	} else {
		fmt.Fprintf( os.Stderr, "Nb de variables dans la section \"serveur\" 8: %d\n", nb )
	}
	vars, err := conf.GetVariables( "serveur", 2 )
	for _, v := range *vars {
		fmt.Fprintf( os.Stderr, "%s = %s\n", v.Name, v.Value )
	}
}
*/
